package com.bcs.competition.speedup;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/actuator/health")
public class FakeActuatorController {

    @GetMapping("/liveness")
    void alive() {
    }

    @GetMapping("/readiness")
    void ready() {
    }
}
