package com.bcs.competition;

import com.bcs.competition.config.setup.BcsCodingDays2021Properties;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties({
        BcsCodingDays2021Properties.class
})
@SpringBootApplication(
        scanBasePackageClasses = BcsCodingDays2021Application.class,
        proxyBeanMethods = false
)
public class BcsCodingDays2021Application {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplicationBuilder()
                .bannerMode(Banner.Mode.OFF) // Ненавижу баннеры ¯\_(ツ)_/¯.
                .web(WebApplicationType.SERVLET)
                .sources(BcsCodingDays2021Application.class)
                .build();

        //noinspection MagicNumber
        application.setApplicationStartup(new BufferingApplicationStartup(10240));

        //noinspection resource
        application.run(args);
    }
}
