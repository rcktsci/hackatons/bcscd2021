package com.bcs.competition.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@EnableScheduling
@Configuration
public class AsyncAndSchedulingConfig {

    private static final int TASK_SCHEDULER_POOL_SIZE = 2;

    @Bean
    @Qualifier("taskScheduler")
    TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setThreadNamePrefix("sched-");

        taskScheduler.setPoolSize(TASK_SCHEDULER_POOL_SIZE);
        taskScheduler.setWaitForTasksToCompleteOnShutdown(true);

        return taskScheduler;
    }
}
