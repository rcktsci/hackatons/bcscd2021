package com.bcs.competition.config;

import org.springframework.boot.web.embedded.undertow.UndertowServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UndertowConfig {

    private static final int UNDERTOW_IO_THREADS = 16;
    private static final int UNDERTOW_WORKER_THREADS = 250;

    @Bean
    public UndertowServletWebServerFactory embeddedServletContainerFactory() {
        UndertowServletWebServerFactory factory = new UndertowServletWebServerFactory();
        factory.setIoThreads(UNDERTOW_IO_THREADS);
        factory.setWorkerThreads(UNDERTOW_WORKER_THREADS);
        return factory;
    }
}
