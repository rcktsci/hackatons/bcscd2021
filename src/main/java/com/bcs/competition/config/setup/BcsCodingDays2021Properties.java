package com.bcs.competition.config.setup;

import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ConfigurationProperties("bcscd2021")
@ConstructorBinding
@Value
@Validated
public class BcsCodingDays2021Properties {

    @NestedConfigurationProperty @NotNull KafkaProperties kafka;
    @NestedConfigurationProperty @NotNull UserCatalogProperties userCatalog;

    @Value
    public static class KafkaProperties {

        @NotBlank String bootstrapAddress;
        @NotBlank String partitions;
    }

    @Value
    public static class UserCatalogProperties {

        @NotBlank String url;
    }
}
