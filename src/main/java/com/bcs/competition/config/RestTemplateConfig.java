package com.bcs.competition.config;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.lang.NonNull;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
public class RestTemplateConfig {

    private static final Duration DEFAULT_CONNECT_TIMEOUT = Duration.ofSeconds(15);

    private static final Duration DEFAULT_READ_TIMEOUT = Duration.ofSeconds(45);

    /**
     * Примерное кол-во хостов на один http-клиент.
     */
    private static final int MAX_HOSTS_PER_CLIENT = 5;

    /**
     * Максимальное кол-во соединений на один хост.
     */
    private static final int MAX_CONNECTIONS_PER_ROUTE = 20;

    @Bean
    public RestTemplate noOpRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        return createNoOpRestTemplate(restTemplateBuilder);
    }

    private static RestTemplate createNoOpRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        RestTemplate restTemplate = createRestTemplate(restTemplateBuilder);
        restTemplate.setErrorHandler(NoOpResponseErrorHandler.INSTANCE);
        return restTemplate;
    }

    private static RestTemplate createRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        HttpClient httpClient = createHttpClient();
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
        return restTemplateBuilder
                .requestFactory(() -> requestFactory)
                .setConnectTimeout(DEFAULT_CONNECT_TIMEOUT)
                .setReadTimeout(DEFAULT_READ_TIMEOUT)
                .build();
    }

    private static HttpClient createHttpClient() {
        HttpClientConnectionManager connectionManager = createPoolingConnectionManager();
        return HttpClientBuilder.create()
                // Отключаем обработку куки.
                .disableCookieManagement()
                // Указываем настроенный Connection Manager.
                .setConnectionManager(connectionManager)
                .build();
    }

    private static HttpClientConnectionManager createPoolingConnectionManager() {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(MAX_HOSTS_PER_CLIENT * MAX_CONNECTIONS_PER_ROUTE);
        connectionManager.setDefaultMaxPerRoute(MAX_CONNECTIONS_PER_ROUTE);
        return connectionManager;
    }

    private static class NoOpResponseErrorHandler extends DefaultResponseErrorHandler {

        /**
         * Обработчик HTTP-статусов ответа, который не выбрасывает исключения
         * <b>HttpClientErrorException</b> и <b>HttpServerErrorException</b>.
         */
        private static final ResponseErrorHandler INSTANCE = new NoOpResponseErrorHandler();

        @Override
        public void handleError(@NonNull ClientHttpResponse response) {
        }
    }
}
