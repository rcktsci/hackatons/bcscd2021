package com.bcs.competition.server.api.portfolio.dto;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class PortfolioEntryDto {

    /**
     * Тикер.
     */
    String ticker;

    /**
     * Количество бумаг в портфеле.
     */
    int balance;

    /**
     * Общая стоимость бумаг этого типа.
     */
    BigDecimal totalCost;

    /**
     * Идентификатор последнего сообщения, по которому изменилось количество бумаг этого типа в портфеле.
     */
    String operationId;
}
