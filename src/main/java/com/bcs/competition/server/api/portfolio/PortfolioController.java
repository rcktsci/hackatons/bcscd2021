package com.bcs.competition.server.api.portfolio;

import com.bcs.competition.server.api.portfolio.dto.PortfolioEntryDto;
import com.bcs.competition.server.api.portfolio.service.PortfolioUiService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/portfolio")
@RequiredArgsConstructor
public class PortfolioController {

    private final PortfolioUiService portfolioUiService;

    @GetMapping(
            value = "/{login}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PortfolioEntryDto> getClientLimits(
            @PathVariable("login") String login
    ) {
        return portfolioUiService.getClientLimits(login);
    }
}
