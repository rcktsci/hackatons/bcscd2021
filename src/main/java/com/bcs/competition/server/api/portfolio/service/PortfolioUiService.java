package com.bcs.competition.server.api.portfolio.service;

import com.bcs.competition.server.api.portfolio.dto.PortfolioEntryDto;
import com.bcs.competition.server.datawarehouse.ClientLimit;
import com.bcs.competition.server.datawarehouse.DwhStorageService;
import com.bcs.competition.server.external.quotes.QuotesService;
import com.bcs.competition.server.external.usercatalog.UserCatalogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class PortfolioUiService {

    private final UserCatalogService userCatalogService;
    private final DwhStorageService dwhStorageService;
    private final QuotesService quotesService;

    /**
     * Концентрированная бизнес-логика.
     */
    public List<PortfolioEntryDto> getClientLimits(String login) {
        // Получаем ID по логину пользователя.
        String clientId = userCatalogService.getClientIdByLogin(login);

        // Получаем из хранилища портфеля текущее содержимое.
        List<ClientLimit> clientLimits = dwhStorageService.getClientLimits(clientId);

        // Какие в нём содержатся тикеры?
        Set<String> clientTickers = extractTickerSet(clientLimits);

        // Запрашиваем по всем тикерам котировки.
        Map<String, BigDecimal> quoteByTickerMap = quotesService.getTickersQuotes(clientTickers);

        // Объединяем результаты.
        return mergeLimitsWithQuotes(clientLimits, quoteByTickerMap);
    }

    private static Set<String> extractTickerSet(Collection<ClientLimit> clientLimits) {
        return StreamEx.of(clientLimits)
                .map(ClientLimit::getTicker)
                .distinct()
                .toImmutableSet();
    }

    private static List<PortfolioEntryDto> mergeLimitsWithQuotes(
            Collection<ClientLimit> clientLimits,
            Map<String, BigDecimal> quoteByTickerMap
    ) {
        return StreamEx.of(clientLimits)
                .map(clientLimit -> populateByQuote(clientLimit, quoteByTickerMap))
                .filter(l -> l.getTotalCost().signum() != 0)
                .toImmutableList();
    }

    private static PortfolioEntryDto populateByQuote(
            ClientLimit clientLimit,
            Map<String, BigDecimal> quoteByTickerMap
    ) {
        String ticker = clientLimit.getTicker();
        int balance = clientLimit.getBalance();

        BigDecimal price = quoteByTickerMap.getOrDefault(ticker, BigDecimal.ZERO);
        BigDecimal totalCost = BigDecimal
                .valueOf(balance)
                .multiply(price)
                .setScale(4, RoundingMode.DOWN);

        return PortfolioEntryDto.builder()
                .ticker(ticker)
                .balance(balance)
                .operationId(clientLimit.getOperationId())
                .totalCost(totalCost)
                .build();
    }
}
