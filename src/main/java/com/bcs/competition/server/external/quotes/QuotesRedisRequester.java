package com.bcs.competition.server.external.quotes;

import org.apache.commons.collections4.SetUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Set;

@Component
public class QuotesRedisRequester {

    private static final String PRICE_FIELD = "price";

    private final HashOperations<String, String, String> hashOperations;

    public QuotesRedisRequester(RedisTemplate<String, String> redisTemplate) {
        this.hashOperations = redisTemplate.opsForHash();
    }

    @NonNull
    public Set<String> getAllKeys() {
        Set<String> keys = hashOperations.getOperations().keys("*");
        return SetUtils.emptyIfNull(keys);
    }

    @NonNull
    public BigDecimal getKeyPriceField(@NonNull String ticker) {
        String value = hashOperations.get(ticker, PRICE_FIELD);
        return StringUtils.isNotBlank(value)
                ? new BigDecimal(value)
                : BigDecimal.ZERO;
    }
}
