package com.bcs.competition.server.external.usercatalog;

import com.bcs.competition.config.setup.BcsCodingDays2021Properties;
import com.bcs.competition.server.external.usercatalog.dto.UserCatalogResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserCatalogService {

    public static final String GET_USER_INFO_URL = "/login-info/";

    private final Map<String, String> clientIdByLogin = new ConcurrentHashMap<>();

    private final RestTemplate restTemplate;
    private final BcsCodingDays2021Properties bcsCodingDays2021Properties;

    public String getClientIdByLogin(String login) {
        return clientIdByLogin.computeIfAbsent(
                login.toLowerCase(),
                k -> fetchClientIdFromRemoteCatalog(login));
    }

    private String fetchClientIdFromRemoteCatalog(String login) {
        String userCatalogHost = bcsCodingDays2021Properties.getUserCatalog().getUrl();
        String url = userCatalogHost + GET_USER_INFO_URL + login;
        UserCatalogResponseDto responseDto = restTemplate.getForObject(url, UserCatalogResponseDto.class);

        String cliendId = Optional.ofNullable(responseDto)
                .map(UserCatalogResponseDto::getClientId)
                .orElseThrow(UserCatalogExceptions.CLIENT_NOT_FOUND_OR_CATALOG_DOWN
                        ::createDefault);
        // log.debug("Client login = {}, ID = {}.", login, cliendId);
        return cliendId;
    }
}
