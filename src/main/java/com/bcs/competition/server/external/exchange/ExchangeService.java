package com.bcs.competition.server.external.exchange;

import com.bcs.competition.server.datawarehouse.DwhStorageService;
import com.bcs.competition.server.external.exchange.dto.MessageDto;
import com.bcs.competition.server.external.exchange.dto.MessageType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExchangeService {

    private final DwhStorageService dwhStorageService;
    private final ObjectMapper objectMapper;

    @KafkaListener(
            topics = {
                    ExchangeTopics.MESSAGES_TOPIC,
            },
            topicPartitions = {
                    @TopicPartition(
                            topic = ExchangeTopics.MESSAGES_TOPIC,
                            partitions = "${bcscd2021.kafka.partitions:0-6}")
            },
            concurrency = "6")
    void onIncomingExchangeMessage(String payload) {
        parsePayloadToMessage(payload)
                // Не нужно обрабатывать в рамках данного сервиса.
                .filter(msg -> msg.getType() != MessageType.REPORT)
                .ifPresent(dwhStorageService::updateClient);
    }

    private Optional<MessageDto> parsePayloadToMessage(String payload) {
        if (payload == null || payload.isBlank()) {
            return Optional.empty();
        }

        try {
            MessageDto result = objectMapper.readValue(payload, MessageDto.class);
            return Optional.of(result);
        } catch (Exception ex) {
            log.warn("Cannot parse MessageDto ({}), skipping: {}", ex.getMessage(), payload);
            return Optional.empty();
        }
    }
}
