package com.bcs.competition.server.external.quotes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;

@Service
@RequiredArgsConstructor
@Slf4j
public class QuotesService {

    private static final ForkJoinPool FJP_BACKGROUND_UPDATER = new ForkJoinPool(8);

    private final QuotesRedisRequester quotesRedisRequester;

    private final Map<String, BigDecimal> priceByTickerMap = new ConcurrentHashMap<>();

    public Map<String, BigDecimal> getTickersQuotes(@NonNull Collection<String> tickers) {
        return StreamEx.of(tickers)
                .mapToEntry(t -> priceByTickerMap.getOrDefault(t, BigDecimal.ZERO))
                .toImmutableMap();
    }

    void updatePrices() {
        Set<String> keys = quotesRedisRequester.getAllKeys();
        StreamEx.of(keys)
                .parallel(FJP_BACKGROUND_UPDATER)
                .mapToEntry(quotesRedisRequester::getKeyPriceField)
                .forKeyValue(priceByTickerMap::put);
    }
}
