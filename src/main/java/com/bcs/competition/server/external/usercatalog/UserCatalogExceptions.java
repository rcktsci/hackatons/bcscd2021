package com.bcs.competition.server.external.usercatalog;

import com.bcs.competition.server.common.exceptions.base.CommonExceptionDescription;
import com.bcs.competition.server.common.exceptions.base.LogType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * Типы исключительных ситуаций, которые могут возникать в приложении.
 */
@AllArgsConstructor
@Getter
public enum UserCatalogExceptions implements CommonExceptionDescription {

    CLIENT_NOT_FOUND_OR_CATALOG_DOWN(LogType.NOTE,
            HttpStatus.BAD_REQUEST,
            "Клиент не найден или лежит сервис каталогов"),
    ;

    /**
     * Каким образом логировать ошибку.
     */
    private final LogType logType;

    /**
     * Какой HTTP статус вернуть клиенту.
     */
    private final HttpStatus httpStatus;

    /**
     * Сообщение по умолчанию.
     * Является префиксом в случае, если в исключении указано собственное сообщение.
     */
    private final String defaultMessage;

    /**
     * Код-название.
     */
    @Override
    public String getName() {
        return name();
    }
}
