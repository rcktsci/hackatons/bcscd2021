package com.bcs.competition.server.external.usercatalog.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class UserCatalogResponseDto {

    /**
     * ID клиента.
     */
    String clientId;

    /**
     * ФИО клиента.
     * Убрал для экономии памяти, извините.
     */
    // String name;

    /**
     * Адрес клиента.
     * Убрал для экономии памяти, извините.
     */
    // String address;
}
