package com.bcs.competition.server.external.exchange.dto;

import lombok.Builder;
import lombok.Value;

/**
 * Ценная бумага в портфеле клиента.
 */
@Value
@Builder
public class MessageLimitDto {

    /**
     * Тикер.
     */
    String ticker;

    /**
     * Количество бумаг в портфеле.
     */
    int balance;
}
