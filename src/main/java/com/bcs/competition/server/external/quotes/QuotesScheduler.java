package com.bcs.competition.server.external.quotes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Profile("!test")
@Component
@RequiredArgsConstructor
@Slf4j
public class QuotesScheduler {

    private final QuotesService quotesService;

    @Scheduled(fixedDelay = 1L)
    void backgroundPriceUpdater() {
        quotesService.updatePrices();
    }
}
