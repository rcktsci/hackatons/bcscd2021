package com.bcs.competition.server.external.exchange.dto;

import lombok.Builder;
import lombok.Value;

import java.time.Instant;
import java.util.List;

/**
 * Сообщение, приходящее от биржи.
 */
@Value
@Builder
public class MessageDto {

    /**
     * Время отправки сообщения.
     */
    Instant timestamp;

    /**
     * Идентификатор операции.
     */
    String operationId;

    /**
     * Идентификатор клиента.
     */
    String clientId;

    /**
     * Тип сообщения.
     * SNAPSHOT содержит актуальное содержимое портфеля на момент отправки сообщения;
     * INCREMENT содержит обновленное количество бумаг определенного типа на момент отправки сообщения;
     * REPORT не нужно обрабатывать в рамках данного сервиса.
     */
    MessageType type;

    /**
     * Список ценных бумаг.
     */
    List<MessageLimitDto> limits;
}
