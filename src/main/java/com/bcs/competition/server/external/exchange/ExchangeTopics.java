package com.bcs.competition.server.external.exchange;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ExchangeTopics {

    public final String MESSAGES_TOPIC = "security-limits";
}
