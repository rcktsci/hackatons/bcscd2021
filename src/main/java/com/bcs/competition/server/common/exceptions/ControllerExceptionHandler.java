package com.bcs.competition.server.common.exceptions;

import com.bcs.competition.server.common.exceptions.base.CommonException;
import com.bcs.competition.server.common.exceptions.base.CommonExceptionHelper;
import com.bcs.competition.server.common.exceptions.base.ErrorResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

/**
 * Обработка исключений, вылетающих из контроллеров.
 */
@RestControllerAdvice
@RequiredArgsConstructor
public class ControllerExceptionHandler {

    private final CommonExceptionHelper commonExceptionHelper;

    /**
     * Ожидаемое исключение. Вероятно, было брошено в коде вручную.
     */
    @ExceptionHandler(CommonException.class)
    public void onCommonException(HttpServletResponse response, CommonException ex) {
        commonExceptionHelper.processCommonExceptionToResponse(response, ex);
    }

    /**
     * Кто-то перепутал GET и PUT.
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponseDto onError(HttpRequestMethodNotSupportedException ex) {
        return commonExceptionHelper.buildErrorResponse(CommonExceptions.UNSUPPORTED, ex);
    }

    /**
     * Произошло что-то неожиданное.
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponseDto onError(Exception ex) {
        return commonExceptionHelper.buildErrorResponse(CommonExceptions.INTERNAL_SERVER_ERROR, ex);
    }
}
