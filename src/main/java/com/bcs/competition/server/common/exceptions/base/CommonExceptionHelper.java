package com.bcs.competition.server.common.exceptions.base;

import com.bcs.competition.server.common.exceptions.CommonExceptions;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.servlet.http.HttpServletResponse;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.UUID;

/**
 * Обработка исключений, вылетающих из контроллеров.
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class CommonExceptionHelper {

    private final ObjectMapper objectMapper;

    /**
     * Позволяет корректно обработать исключение в потоке, который не
     * является обработчиком внешнего запроса.
     */
    public void consumeException(Throwable throwable) {
        try {
            if (throwable instanceof CommonException) {
                CommonException ce = (CommonException) throwable;
                CommonExceptionDescription description = ce.getErrorType();
                buildErrorResponse(description, throwable);
                return;
            }

            buildErrorResponse(CommonExceptions.INTERNAL_SERVER_ERROR, throwable);
        } catch (Exception ex) {
            log.error("Cannot handle error.", ex);
        }
    }

    /**
     * Преобразование исключения в DTO для клиента и его непосредственный вывод.
     */
    @SneakyThrows
    public void processCommonExceptionToResponse(HttpServletResponse response, CommonException ex) {
        CommonExceptionDescription description = Optional.of(ex)
                .map(CommonException::getErrorType)
                .orElse(CommonExceptions.INTERNAL_SERVER_ERROR);

        ErrorResponseDto dto = buildErrorResponse(description, ex);

        String contentBody = objectMapper.writeValueAsString(dto);
        byte[] contentBytes = contentBody.getBytes(StandardCharsets.UTF_8);
        response.setStatus(description.getHttpStatus().value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setContentLength(contentBytes.length);

        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(contentBytes)) {
            byteArrayInputStream.transferTo(response.getOutputStream());
        }
    }

    /**
     * Сработала валидация Spring.
     */
    public ErrorResponseDto buildErrorResponse(MethodArgumentNotValidException ex) {
        String message = StreamEx.of(ex.getBindingResult().getFieldErrors())
                .map(fe -> fe.getField() + ' ' + fe.getDefaultMessage())
                .ifEmpty("Некорректное значение.")
                .joining(", ");

        CommonException exception = CommonExceptions.VALIDATION_FAILED.creator()
                .message(message)
                .cause(ex)
                .create();
        return buildErrorResponse(CommonExceptions.VALIDATION_FAILED, exception);
    }

    /**
     * Преобразование исключения в DTO для клиентской стороны (МП / т.п.),
     * и параллельно его логирование в Graylog / мессенджеры, при необходимости.
     */
    public ErrorResponseDto buildErrorResponse(CommonExceptionDescription errorType, Throwable ex) {
        String errorMessage = ex.getMessage();

        ErrorResponseDto.ErrorResponseDtoBuilder responseDtoBuilder = ErrorResponseDto.builder()
                .code(errorType.getName())
                .message(errorMessage);

        switch (errorType.getLogType()) {
            case SKIP:
                return responseDtoBuilder.build();
            case NOTE:
                log.warn("{}", errorMessage);
                return responseDtoBuilder.build();
            case ERROR:
                UUID errorUniqueId = UUID.randomUUID();
                log.error("#### Exception UUID: {} ####\n{}", errorUniqueId, errorMessage, ex);
                return responseDtoBuilder.build();
            default:
                throw new IllegalArgumentException();
        }
    }
}
