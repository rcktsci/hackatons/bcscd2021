package com.bcs.competition.server.common.exceptions.base;

import lombok.Builder;
import lombok.Value;

/**
 * Описание произошедшей ошибки.
 */
@Value
@Builder
public class ErrorResponseDto {

    /**
     * Машино-читаемый код ошибки.
     */
    String code;

    /**
     * Текстовое описание ситуации.
     */
    String message;
}
