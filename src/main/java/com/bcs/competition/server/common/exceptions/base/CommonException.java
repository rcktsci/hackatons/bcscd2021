package com.bcs.competition.server.common.exceptions.base;

import lombok.Getter;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * Базовое исключение, которое предполагается бросать в приложении.
 */
@SuppressWarnings("serial")
@Getter
public class CommonException extends RuntimeException {

    private final CommonExceptionDescription errorType;

    public CommonException(CommonExceptionDescription errorType, String message, Exception cause) {
        super(message, cause);
        this.errorType = Objects.requireNonNull(errorType);
    }

    @Override
    public String getMessage() {
        String defaultMessage = errorType.getDefaultMessage();
        String currentMessage = super.getMessage();
        return StreamEx.of(defaultMessage, currentMessage)
                .map(StringUtils::trimToNull)
                .nonNull()
                .joining(": ");
    }
}
