package com.bcs.competition.server.common.exceptions;

import com.bcs.competition.server.common.exceptions.base.CommonExceptionDescription;
import com.bcs.competition.server.common.exceptions.base.LogType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * Типы исключительных ситуаций, которые могут возникать в приложении.
 */
@AllArgsConstructor
@Getter
public enum CommonExceptions implements CommonExceptionDescription {

    NOT_FOUND(LogType.SKIP,
            HttpStatus.NOT_FOUND,
            "Некорректный URI запроса"),

    /**
     * Функционал не поддерживается (и не планирует этого делать).
     */
    UNSUPPORTED(
            LogType.NOTE,
            HttpStatus.BAD_REQUEST,
            "Функционал не поддерживается"),

    /**
     * Значение задано некорректно.
     */
    VALIDATION_FAILED(
            LogType.NOTE,
            HttpStatus.BAD_REQUEST,
            "Ошибка валидации"),

    /**
     * Функционал ещё не реализован.
     * Возможно, ждём правок от OpenAPI, DESS или любой другой команды СКБ.
     * Или сами не успели.
     */
    NOT_IMPLEMENTED_YET(
            LogType.NOTE,
            HttpStatus.NOT_IMPLEMENTED,
            "Функционал ещё не реализован"),

    /**
     * Любая другая исключительная ситуация, но не очень серьёзная.
     * Например, клиент закрыл соединение (оборвался) до окончания чтения запроса / записи ответа.
     */
    INTERNAL_SERVER_WARNING(LogType.SKIP,
            HttpStatus.I_AM_A_TEAPOT,
            "Возникла неожиданная, но некритичная ситуация"),

    /**
     * Любая другая исключительная ситуация.
     */
    INTERNAL_SERVER_ERROR(LogType.ERROR,
            HttpStatus.INTERNAL_SERVER_ERROR,
            "Внутренняя ошибка сервера"),
    ;

    /**
     * Каким образом логировать ошибку.
     */
    private final LogType logType;

    /**
     * Какой HTTP статус вернуть клиенту.
     */
    private final HttpStatus httpStatus;

    /**
     * Сообщение по умолчанию.
     * Является префиксом в случае, если в исключении указано собственное сообщение.
     */
    private final String defaultMessage;

    /**
     * Код-название.
     */
    @Override
    public String getName() {
        return name();
    }
}
