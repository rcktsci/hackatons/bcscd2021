package com.bcs.competition.server.common.exceptions.base;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.http.HttpStatus;

/**
 * Интерфейс описания исключения.
 */
public interface CommonExceptionDescription {

    /**
     * Уникальный код исключения. Обычно это name() типа перечисления (enum), который реализует данный интерфейс.
     * <br/>
     * Например, FORBIDDEN.
     */
    String getName();

    /**
     * Какой HTTP-статус необходимо вернуть клиенту при возникновении данного исключения.
     * <br/>
     * Иногда это может быть даже HttpStatus.OK.
     */
    HttpStatus getHttpStatus();

    /**
     * Сообщение, отправляемое клиенту по умолчанию.
     * <br/>
     * Если в исключении имеется собственная информация, то они будут сконкатенированы: "defaultMessage: message".
     */
    String getDefaultMessage();

    /**
     * Как следует логировать данное исключение в самом приложении?
     * <br/>
     * Агрессивно, слабо или вообще игнорировать?
     */
    LogType getLogType();

    /**
     * Фабрика исключений с дефолтными параметрами.
     */
    default CommonException createDefault() {
        CommonExceptionCreator creator = new CommonExceptionCreator(this);
        return creator.create();
    }

    /**
     * Фабрика билдера исключений.
     */
    default CommonExceptionCreator creator() {
        return new CommonExceptionCreator(this);
    }

    /**
     * Билдер обще-приложенческих исключений.
     */
    @RequiredArgsConstructor
    @Accessors(fluent = true, chain = true)
    class CommonExceptionCreator {

        private final CommonExceptionDescription description;

        private String message;

        /**
         * Причина выбрасывания исключения.
         */
        @Setter
        private Exception cause;

        /**
         * <p>Сообщение об ошибке.</p>
         */
        public CommonExceptionCreator message(String text) {
            this.message = text;
            return this;
        }

        /**
         * <p>Сообщение об ошибке.</p>
         * <p>Поддерживает форматирование как <b>String.format()</b>.</p>
         */
        public CommonExceptionCreator message(String format, Object... args) {
            this.message = args.length > 0
                    ? String.format(format, args)
                    : format;
            return this;
        }

        /**
         * Создать объект исключения.
         */
        public CommonException create() {
            return new CommonException(description, message, cause);
        }
    }
}
