package com.bcs.competition.server.common.exceptions;

import com.bcs.competition.server.common.exceptions.base.CommonExceptionHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Контроллер, обрабатывающий 404 и прочие стандартные ошибки.
 */
@RestController
@RequiredArgsConstructor
public class ApplicationErrorController implements ErrorController {

    private final CommonExceptionHelper commonExceptionHelper;

    @RequestMapping(value = "/error", method = {
            RequestMethod.GET,
            RequestMethod.POST,
            RequestMethod.PUT,
            RequestMethod.DELETE
    })
    public void handleError(HttpServletRequest request, HttpServletResponse response) {
        commonExceptionHelper.processCommonExceptionToResponse(
                response,
                CommonExceptions.NOT_FOUND.creator()
                        .message(request.getRequestURI())
                        .create());
    }
}
