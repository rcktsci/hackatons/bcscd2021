package com.bcs.competition.server.datawarehouse;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ClientLimit {

    /**
     * Тикер.
     */
    String ticker;

    /**
     * Количество бумаг в портфеле.
     */
    int balance;

    /**
     * Идентификатор последнего сообщения, по которому изменилось количество бумаг этого типа в портфеле.
     */
    String operationId;
}
