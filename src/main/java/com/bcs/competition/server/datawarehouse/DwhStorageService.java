package com.bcs.competition.server.datawarehouse;

import com.bcs.competition.server.external.exchange.dto.MessageDto;

import java.util.List;

public interface DwhStorageService {

    void updateClient(MessageDto message);

    List<ClientLimit> getClientLimits(String clientId);
}
