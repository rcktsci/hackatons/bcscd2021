package com.bcs.competition.server.datawarehouse;

import com.bcs.competition.server.external.exchange.dto.MessageDto;
import com.bcs.competition.server.external.exchange.dto.MessageLimitDto;
import com.bcs.competition.server.external.exchange.dto.MessageType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

@Service
@RequiredArgsConstructor
@Slf4j
class InMemoryDWH implements DwhStorageService {

    // Геттер нужен для тестирования.
    @Getter(AccessLevel.PACKAGE)
    private final Map<String, DwhContext> contextMap = new ConcurrentHashMap<>();

    @Override
    public void updateClient(MessageDto message) {
        try (DwhContext context = getClientContext(message.getClientId())) {
            updateClientContext(context, message);
        }
    }

    private static void updateClientContext(DwhContext context, MessageDto message) {
        String operationId = message.getOperationId();
        Instant timestamp = message.getTimestamp();

        Map<String, TickerContext> tickerUpdates = StreamEx.of(message.getLimits())
                .mapToEntry(
                        MessageLimitDto::getTicker,
                        l -> {
                            TickerContext tickerContext = new TickerContext();
                            tickerContext.setOperationId(operationId);
                            tickerContext.setTimestamp(timestamp);
                            tickerContext.setBalance(l.getBalance());
                            return tickerContext;
                        })
                .toImmutableMap();

        Map<String, TickerContext> clientTickerMap = context.getTickerMap();
        EntryStream.of(tickerUpdates)
                .forKeyValue((ticker, newContext) -> {
                    TickerContext oldContext = clientTickerMap.get(ticker);
                    if (oldContext == null || oldContext.getTimestamp().isBefore(newContext.getTimestamp())) {
                        clientTickerMap.put(ticker, newContext);
                    }
                });

        // Если это снапшот, то нужно удалить все бумаги, которых в нём нет и которые старше снапшота.
        if (message.getType() == MessageType.SNAPSHOT) {
            List<String> removeTooOldTickets = EntryStream.of(clientTickerMap)
                    .keys()
                    .filter(t -> !tickerUpdates.containsKey(t))
                    .mapToEntry(clientTickerMap::get)
                    .nonNullValues()
                    .mapValues(TickerContext::getTimestamp)
                    .filterValues(timestamp::isAfter)
                    .keys()
                    .toList();
            removeTooOldTickets.forEach(clientTickerMap::remove);
        }
    }

    @Override
    public List<ClientLimit> getClientLimits(String clientId) {
        try (DwhContext context = getClientContext(clientId)) {
            Map<String, TickerContext> clientTickerMap = context.getTickerMap();

            return EntryStream.of(clientTickerMap)
                    .filterValues(v -> v.getBalance() > 0)
                    .mapKeyValue((k, v) -> ClientLimit.builder()
                            .ticker(k)
                            .balance(v.getBalance())
                            .operationId(v.getOperationId())
                            .build())
                    .toImmutableList();
        }
    }

    private DwhContext getClientContext(String clientId) {
        DwhContext context = contextMap.computeIfAbsent(
                clientId,
                k -> new DwhContext());
        context.getLock().lock();
        return context;
    }

    @Data
    private static class DwhContext implements AutoCloseable {

        private Map<String, TickerContext> tickerMap = new HashMap<>();
        private ReentrantLock lock = new ReentrantLock();

        @Override
        public void close() {
            lock.unlock();
        }
    }

    @Data
    private static class TickerContext {

        private String operationId = UUID.randomUUID().toString();
        private Instant timestamp = Instant.EPOCH;
        private int balance;
    }
}
