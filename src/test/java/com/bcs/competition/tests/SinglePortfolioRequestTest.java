package com.bcs.competition.tests;

import com.bcs.competition.BaseApplicationTest;
import com.bcs.competition.config.ExchangeProxyConfig;
import com.bcs.competition.server.api.portfolio.dto.PortfolioEntryDto;
import com.bcs.competition.server.datawarehouse.InMemoryDwhTestHelper;
import com.bcs.competition.server.external.exchange.KafkaTestSender;
import com.bcs.competition.server.external.exchange.dto.MessageDto;
import com.bcs.competition.server.external.exchange.dto.MessageLimitDto;
import com.bcs.competition.server.external.exchange.dto.MessageType;
import com.bcs.competition.server.external.quotes.QuotesFakeScheduler;
import com.bcs.competition.server.external.usercatalog.UserCatalogService;
import com.bcs.competition.server.external.usercatalog.dto.UserCatalogResponseDto;
import com.bcs.competition.utils.TestUtils;
import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.testcontainers.shaded.org.awaitility.Awaitility;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@Slf4j
public class SinglePortfolioRequestTest extends BaseApplicationTest {

    private static final String PORTFOLIO_URL = "/portfolio/{login}";

    private static final ParameterizedTypeReference<List<PortfolioEntryDto>> PORTFOLIO_RESPONSE_TYPE = new ParameterizedTypeReference<>() {

    };

    private static final String CLIENT_1_LOGIN = "T-1000";
    private static final String CLIENT_2_LOGIN = "Bishop";

    private static final String CLIENT_1_ID = "bf020992-163a-4a86-a035-f210f9e523bf";
    private static final String CLIENT_2_ID = "eeb1b4ff-ae42-4221-b607-d2c28ad9d067";

    private static final String TICKER_1 = "ABCD";
    private static final String TICKER_2 = "WXYZ";

    private static final String OPERATION_1_ID = "419d72dc-e741-41b5-91ac-1079602762be";
    private static final String OPERATION_2_ID = "a57f0dbc-69ab-435f-8499-78ceec5ccf1c";
    private static final String OPERATION_3_ID = "f5c9d502-d787-4f06-b378-446f76b63566";
    private static final String OPERATION_4_ID = "fd1b3432-beb0-46f1-870b-e7aa182b4b62";
    private static final String OPERATION_5_ID = "45a72337-0e86-4420-a7b6-5b639f783f83";

    @Autowired
    private KafkaTestSender kafkaTestSender;
    @Autowired
    private ExchangeProxyConfig.DwhStorageServiceCountingProxy dwhStorageServiceCountingProxy;
    @Autowired
    private QuotesFakeScheduler quotesFakeScheduler;
    @Autowired
    private InMemoryDwhTestHelper inMemoryDwhTestHelper;

    @Test
    void testLifecycleClient1() {

        // GIVEN

        mockTickersPrices();

        mockClientInUserCatalog(CLIENT_1_LOGIN, CLIENT_1_ID);

        prepareKafkaData("test-data/lifecycle-test-kafka-stream.json");

        // EXECUTE

        List<PortfolioEntryDto> response = executeApiCall(CLIENT_1_LOGIN);

        // ASSERT

        assertThat(response).hasSize(2);

        Map<String, PortfolioEntryDto> responseByTicker = StreamEx.of(response)
                .mapToEntry(
                        PortfolioEntryDto::getTicker,
                        Function.identity())
                .toMap();
        PortfolioEntryDto ticker1Response = responseByTicker.get(TICKER_1);
        PortfolioEntryDto ticker2Response = responseByTicker.get(TICKER_2);
        assertThat(ticker1Response).isNotNull();
        assertThat(ticker2Response).isNotNull();

        assertThat(ticker1Response.getOperationId()).isEqualTo(OPERATION_4_ID);
        assertThat(ticker1Response.getBalance()).isEqualByComparingTo(5);
        assertThat(ticker1Response.getTotalCost()).isEqualByComparingTo("5");

        assertThat(ticker2Response.getOperationId()).isEqualTo(OPERATION_4_ID);
        assertThat(ticker2Response.getBalance()).isEqualByComparingTo(6);
        assertThat(ticker2Response.getTotalCost()).isEqualByComparingTo("60");
    }

    @Test
    void testLifecycleClient2() {

        // GIVEN

        mockTickersPrices();

        mockClientInUserCatalog(CLIENT_2_LOGIN, CLIENT_2_ID);

        prepareKafkaData("test-data/lifecycle-test-kafka-stream.json");

        // EXECUTE

        List<PortfolioEntryDto> response = executeApiCall(CLIENT_2_LOGIN);

        // ASSERT

        assertThat(response).hasSize(1);

        PortfolioEntryDto ticker1Response = StreamEx.of(response)
                .findFirst()
                .orElseThrow();
        assertThat(ticker1Response).isNotNull();

        assertThat(ticker1Response.getOperationId()).isEqualTo(OPERATION_3_ID);
        assertThat(ticker1Response.getBalance()).isEqualByComparingTo(10);
        assertThat(ticker1Response.getTotalCost()).isEqualByComparingTo("10");
    }

    private void mockClientInUserCatalog(String login, String clientId) {
        UserCatalogResponseDto userCatalogResponseDto = UserCatalogResponseDto.builder()
                .clientId(clientId)
                .build();
        getUserCatalogServerMock()
                .stubFor(
                        get(UserCatalogService.GET_USER_INFO_URL + login)
                                .willReturn(aResponse()
                                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                                        .withBody(toJson(userCatalogResponseDto))));
    }

    private void mockTickersPrices() {
        doReturn(Set.of(TICKER_1, TICKER_2))
                .when(quotesRedisRequester)
                .getAllKeys();

        doReturn(BigDecimal.ONE)
                .when(quotesRedisRequester)
                .getKeyPriceField(TICKER_1);

        doReturn(BigDecimal.TEN)
                .when(quotesRedisRequester)
                .getKeyPriceField(TICKER_2);

        quotesFakeScheduler.updatePricesRightNow();
    }

    private void prepareKafkaData(String filename) {
        // Вставляемые данные удобнее описывать в .json формате.
        KafkaTestData kafkaTestData = TestUtils.getResource(filename, KafkaTestData.class);

        // Очищаем данные перед наполнением.
        inMemoryDwhTestHelper.clear();
        dwhStorageServiceCountingProxy.resetCounter();

        Instant baseTime = Instant.now();

        var entries = kafkaTestData.getEntries();
        for (KafkaTestData.KafkaTestDataEntry entry : entries) {
            // Добавляю фейковый лимит на несуществующем тикере,
            // чтобы одинаковые сообщения в разных тестах немного отличались.
            List<MessageLimitDto> limits = StreamEx.of(entry.getLimits())
                    .append(MessageLimitDto.builder()
                            .ticker(UUID.randomUUID().toString())
                            .balance(0)
                            .build())
                    .toList();

            MessageDto message = MessageDto.builder()
                    .timestamp(baseTime.plusSeconds(entry.getBaseTimeOffset()))
                    .operationId(entry.getOperationId())
                    .clientId(entry.getClientId())
                    .type(entry.isSnapshot()
                            ? MessageType.SNAPSHOT
                            : MessageType.INCREMENT)
                    .limits(limits)
                    .build();

            kafkaTestSender.send(message);
        }

        // Ожидаем, пока приложение не обработает все данные из Kafka.
        Awaitility.await()
                .atMost(Duration.ofSeconds(5))
                .pollDelay(Duration.ofMillis(100))
                .until(() -> dwhStorageServiceCountingProxy.getUpdateClientCount() == entries.size());
    }

    private List<PortfolioEntryDto> executeApiCall(String login) {
        ResponseEntity<List<PortfolioEntryDto>> responseEntity = getTestApi().exchange(
                PORTFOLIO_URL,
                HttpMethod.GET,
                null,
                PORTFOLIO_RESPONSE_TYPE,
                Collections.singletonMap("login", login));

        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

        List<PortfolioEntryDto> body = responseEntity.getBody();
        assertThat(body)
                .isNotNull()
                .doesNotContainNull();

        return body;
    }

    @Value
    @Builder
    private static class KafkaTestData {

        @Singular
        List<KafkaTestDataEntry> entries;

        @Value
        @Builder
        private static class KafkaTestDataEntry {

            String clientId;
            String operationId;
            boolean snapshot;
            int baseTimeOffset;

            @Singular
            List<MessageLimitDto> limits;
        }
    }
}
