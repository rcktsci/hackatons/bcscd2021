package com.bcs.competition.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

import java.util.Map;

public class KafkaContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private final KafkaContainer container = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:7.0.0"));

    @Override
    public void initialize(@NotNull ConfigurableApplicationContext applicationContext) {
        container.start();

        Map<String, String> kafkaProperties = Map.of(
                "kafka_host", container.getContainerIpAddress(),
                "kafka_port", String.valueOf(container.getMappedPort(9093)),
                "bcscd2021.kafka.partitions", "0");
        TestPropertyValues
                .of(kafkaProperties)
                .applyTo(applicationContext.getEnvironment());
    }
}
