package com.bcs.competition.config;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.lang.NonNull;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Поднятие контекста — дорогое удовольствие, поэтому нужно убедиться, что за один прогон всех
 * имеющихся тестов мы не тратим время на поднятие контекста более, чем один раз.
 * Этот контекст должен быть наполнен заранее всем, чем нужно.
 */
public class FailFastContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final AtomicBoolean FLAG = new AtomicBoolean();

    @Override
    public void initialize(@NonNull ConfigurableApplicationContext applicationContext) {
        boolean error = FLAG.getAndSet(true);
        if (error) {
            throw new IllegalStateException("Second application context start attempt.");
        }
    }
}
