package com.bcs.competition.config;

import com.bcs.competition.config.setup.BcsCodingDays2021Properties;
import com.bcs.competition.server.external.exchange.ExchangeTopics;
import com.bcs.competition.server.external.exchange.dto.MessageDto;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@TestConfiguration
@RequiredArgsConstructor
public class KafkaTestConfig {

    @Bean
    public ProducerFactory<String, MessageDto> testProducerFactory(BcsCodingDays2021Properties appProperties) {
        Map<String, Object> properties = new HashMap<>();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, appProperties.getKafka().getBootstrapAddress());
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(properties);
    }

    @Bean
    public KafkaTemplate<String, MessageDto> testKafkaTemplate(
            ProducerFactory<String, MessageDto> testProducerFactory
    ) {
        return new KafkaTemplate<>(testProducerFactory);
    }
}
