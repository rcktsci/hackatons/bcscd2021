package com.bcs.competition.config;

import com.bcs.competition.server.datawarehouse.ClientLimit;
import com.bcs.competition.server.datawarehouse.DwhStorageService;
import com.bcs.competition.server.external.exchange.dto.MessageDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.util.List;

@TestConfiguration
public class ExchangeProxyConfig {

    @Primary
    @Bean
    DwhStorageService proxyDwhStorageService(DwhStorageService original) {
        return new DwhStorageServiceCountingProxy(original);
    }

    @RequiredArgsConstructor
    public static class DwhStorageServiceCountingProxy implements DwhStorageService {

        private final DwhStorageService original;

        @Getter
        private int updateClientCount;

        @Override
        public void updateClient(MessageDto message) {
            original.updateClient(message);
            updateClientCount += 1;
        }

        @Override
        public List<ClientLimit> getClientLimits(String clientId) {
            return original.getClientLimits(clientId);
        }

        public void resetCounter() {
            updateClientCount = 0;
        }
    }
}
