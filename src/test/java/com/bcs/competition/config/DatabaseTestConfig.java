package com.bcs.competition.config;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.sql.DataSource;

import java.util.Map;

@TestConfiguration
@RequiredArgsConstructor
@Slf4j
public class DatabaseTestConfig {

    @UtilityClass
    public static class Database {

        private final String IMAGE = "postgres:14-alpine";

        private final Map<String, String> TMPFS_MAP = Map.of("/var/lib/postgresql/", "rw");

        private final String DATABASE = "e2e_tests";
        private final String USERNAME = "user";
        private final String PASSWORD = "test";
    }

    private static final ApplicationDbContainer DATABASE_CONTAINER = new ApplicationDbContainer(Database.IMAGE)
            .withTmpFs(Database.TMPFS_MAP)
            .withDatabaseName(Database.DATABASE)
            .withUsername(Database.USERNAME)
            .withPassword(Database.PASSWORD);

    @Primary
    @Bean
    public DataSource testDataSource() {
        DATABASE_CONTAINER.start();

        return DataSourceBuilder.create()
                .url(DATABASE_CONTAINER.getJdbcUrl())
                .username(Database.USERNAME)
                .password(Database.PASSWORD)
                .build();
    }

    private static final class ApplicationDbContainer extends PostgreSQLContainer<ApplicationDbContainer> {

        private ApplicationDbContainer(String dockerImageName) {
            super(dockerImageName);
        }
    }
}
