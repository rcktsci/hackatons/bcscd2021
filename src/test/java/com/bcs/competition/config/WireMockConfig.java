package com.bcs.competition.config;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.Options;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.lang.NonNull;

import java.util.Map;

@TestConfiguration
@RequiredArgsConstructor
@Slf4j
public class WireMockConfig implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final WireMockServer USER_CATALOG_MOCK = createWireMockInstance();

    private static WireMockServer createWireMockInstance() {
        return new WireMockServer(Options.DYNAMIC_PORT);
    }

    @Override
    public void initialize(@NonNull ConfigurableApplicationContext applicationContext) {
        USER_CATALOG_MOCK.start();
        int wireMockAdPort = USER_CATALOG_MOCK.port();
        log.info("WireMock server for user-catalog started on port {}.", wireMockAdPort);

        Map<String, String> properties = Map.of(
                "bcscd2021.user-catalog.url", String.format("http://localhost:%d", wireMockAdPort));
        TestPropertyValues
                .of(properties)
                .applyTo(applicationContext.getEnvironment());
    }

    @Bean
    @Qualifier("user-catalog")
    public WireMockServer userCatalogWireMock() {
        return USER_CATALOG_MOCK;
    }
}
