package com.bcs.competition.utils;

import com.bcs.competition.BaseApplicationTest;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@UtilityClass
@Slf4j
public class TestUtils {

    private final ObjectMapper objectMapper = JsonMapper.builder()
            .enable(JsonReadFeature.ALLOW_JAVA_COMMENTS)
            .enable(SerializationFeature.INDENT_OUTPUT)
            .build()
            .findAndRegisterModules();

    /**
     * Достаёт объект указанного класса из JSON-файла в ресурсах.
     */
    @SneakyThrows
    public <T> T getResource(String filename, Class<T> clazz) {
        try (InputStream inputStream = BaseApplicationTest.class.getResourceAsStream(filename)) {
            assertResourceInputStream(inputStream, filename);
            return objectMapper.readValue(inputStream, clazz);
        }
    }

    /**
     * Достаёт объект указанного типа из JSON-файла в ресурсах.
     */
    @SneakyThrows
    public <T> T getResource(String filename, TypeReference<T> type) {
        try (InputStream inputStream = BaseApplicationTest.class.getResourceAsStream(filename)) {
            assertResourceInputStream(inputStream, filename);
            return objectMapper.readValue(inputStream, type);
        }
    }

    /**
     * Читает файл в ресурсах в строку.
     */
    public String getResourceAsString(String filename) {
        try (InputStream inputStream = BaseApplicationTest.class.getResourceAsStream(filename)) {
            assertResourceInputStream(inputStream, filename);
            return new String(Objects.requireNonNull(inputStream).readAllBytes(), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            log.error("", ex);
        }

        return null;
    }

    private void assertResourceInputStream(InputStream inputStream, String filename) throws IOException {
        assertThat(inputStream)
                .overridingErrorMessage("Classpath resource not found: '%s'.", filename)
                .isNotNull();
        assertThat(inputStream.available())
                .isNotZero();
    }
}
