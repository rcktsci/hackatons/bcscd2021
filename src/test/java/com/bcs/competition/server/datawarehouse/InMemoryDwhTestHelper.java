package com.bcs.competition.server.datawarehouse;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class InMemoryDwhTestHelper {

    private final InMemoryDWH dataWarehouse;

    public void clear() {
        dataWarehouse.getContextMap().clear();
    }
}
