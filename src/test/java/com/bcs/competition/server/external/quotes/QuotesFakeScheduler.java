package com.bcs.competition.server.external.quotes;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class QuotesFakeScheduler {

    private final QuotesService quotesService;

    public void updatePricesRightNow() {
        quotesService.updatePrices();
    }
}
