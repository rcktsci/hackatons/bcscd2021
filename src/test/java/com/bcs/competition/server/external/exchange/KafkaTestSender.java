package com.bcs.competition.server.external.exchange;

import com.bcs.competition.server.external.exchange.dto.MessageDto;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class KafkaTestSender {

    private final KafkaTemplate<String, MessageDto> testKafkaTemplate;

    public void send(MessageDto response) {
        testKafkaTemplate.send(ExchangeTopics.MESSAGES_TOPIC, response);
    }
}
