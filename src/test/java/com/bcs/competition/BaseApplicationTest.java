package com.bcs.competition;

import com.bcs.competition.config.ExchangeProxyConfig;
import com.bcs.competition.config.FailFastContextInitializer;
import com.bcs.competition.config.KafkaContextInitializer;
import com.bcs.competition.config.KafkaTestConfig;
import com.bcs.competition.config.WireMockConfig;
import com.bcs.competition.server.external.quotes.QuotesRedisRequester;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {
                // DatabaseTestConfig.class,
                WireMockConfig.class,
                KafkaTestConfig.class,
                ExchangeProxyConfig.class
        })
@ContextConfiguration(initializers = {
        FailFastContextInitializer.class,
        WireMockConfig.class,
        KafkaContextInitializer.class
})
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class BaseApplicationTest {

    @Autowired
    @Qualifier("user-catalog")
    @Getter(AccessLevel.PROTECTED)
    private WireMockServer userCatalogServerMock;

    @Autowired
    @Getter(AccessLevel.PROTECTED)
    private TestRestTemplate testApi;

    @Autowired
    private ObjectMapper objectMapper;

    @SpyBean
    protected QuotesRedisRequester quotesRedisRequester;

    @AfterEach
    void resetAll() {
        userCatalogServerMock.resetAll();
    }

    @SneakyThrows
    protected String toJson(Object payload) {
        return objectMapper.writeValueAsString(payload);
    }
}
